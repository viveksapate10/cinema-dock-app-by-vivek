﻿using AutoMapper;
using CinemaDock.DTOs.ActorDTO;
using CinemaDock.DTOs.MovieDTO;
using CinemaDock.Models;

namespace CinemaDock.Profiles
{
    public class MovieProfile:Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>();

            CreateMap<MovieCreateDTO, Movie>();

            CreateMap<MovieEditDTO, Movie>();
        }

    }
}
