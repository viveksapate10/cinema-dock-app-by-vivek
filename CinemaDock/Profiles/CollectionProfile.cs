﻿using AutoMapper;
using CinemaDock.DTOs.ActorDTO;
using CinemaDock.DTOs.CollectionDTO;
using CinemaDock.Models;

namespace CinemaDock.Profiles
{
    public class CollectionProfile:Profile
    {
        public CollectionProfile()
        {
            CreateMap<Collection, CollectionReadDTO>();

            CreateMap<CollectionCreateDTO, Collection>();

            CreateMap<CollectionEditDTO, Collection>();
        }

    }
}
