﻿using AutoMapper;
using CinemaDock.DTOs.ActorDTO;
using CinemaDock.Models;

namespace CinemaDock.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor , ActorReadDTO>();

            CreateMap<ActorCreateDTO , Actor>();

            CreateMap<ActorEditDTO,Actor>();
        }
    }
}
