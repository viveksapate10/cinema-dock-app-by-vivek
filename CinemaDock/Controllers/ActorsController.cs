﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CinemaDock.Models;
using CinemaDock.Models.Data;
using AutoMapper;
using CinemaDock.DTOs.ActorDTO;

namespace CinemaDock.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class ActorsController : ControllerBase
    {
        private readonly ActorMoviesDbContext _context;
        private readonly IMapper _mapper;

        public ActorsController(ActorMoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        /// <summary>
        /// Get All The Actors From Actor Table
        /// </summary>
        /// <returns>Return All The Actors From Actor DTO</returns>
        // GET: api/Actors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorReadDTO>>> GetActors()
        {
          if (_context.Actors == null)
          {
              return NotFound();
          }
            var actorModel= await _context.Actors.ToListAsync();
            var actorDto= _mapper.Map<List<ActorReadDTO>>(actorModel);
            return actorDto;
        }

        /// <summary>
        /// Get A Single Actor From Actor Table by Searching Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a Single Actor From DTO </returns>
        // GET: api/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorReadDTO>> GetActor(int id)
        {
          if (_context.Actors == null)
          {
              return NotFound();
          }
            var actor = await _context.Actors.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }

            return _mapper.Map<ActorReadDTO>(actor);
        }

        /// <summary>
        /// Update An Actor From Actor Table By Id And Name
        /// </summary>
        /// <param name="id"></param>
        /// <param name="actor"></param>
        /// <returns>Return the Update </returns>
        // PUT: api/Actors/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, ActorEditDTO actor)
        {
            if (id != actor.ActorId)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add An Actor To Actor Table
        /// </summary>
        /// <param name="actorDto"></param>
        /// <returns>return the Adding</returns>
        // POST: api/Actors
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ActorReadDTO>> PostActor(ActorCreateDTO actorDto)
        {
          if (_context.Actors == null)
          {
              return Problem("Entity set 'ActorMoviesDbContext.Actors'  is null.");
          }

           var actorModel = _mapper.Map<Actor>(actorDto);

            _context.Actors.Add(actorModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actorModel.ActorId }, actorDto);
        }

        /// <summary>
        /// Delete an Actor from Actor Table
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns Delete</returns>
        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteActor(int id)
        {
            if (_context.Actors == null)
            {
                return NotFound();
            }
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ActorExists(int id)
        {
            return (_context.Actors?.Any(e => e.ActorId == id)).GetValueOrDefault();
        }
    }
}
