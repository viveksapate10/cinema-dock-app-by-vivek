﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CinemaDock.Models;
using CinemaDock.Models.Data;
using AutoMapper;
using CinemaDock.DTOs.CollectionDTO;
using CinemaDock.DTOs.ActorDTO;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System.Numerics;

namespace CinemaDock.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class CollectionsController : ControllerBase
    {
        private readonly ActorMoviesDbContext _context;
        private readonly IMapper _mapper;

        public CollectionsController(ActorMoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        /// <summary>
        /// Get All The Collections From Collection Table
        /// </summary>
        /// <returns></returns>
        // GET: api/Collections
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CollectionReadDTO>>> GetCollections()
        {
          if (_context.Collections == null)
          {
              return NotFound();
          }
            var collectionModel= await _context.Collections.ToListAsync();
            var collectionDto= _mapper.Map<List<CollectionReadDTO>>(collectionModel);
            return collectionDto;
        }

        /// <summary>
        /// Get A single Collection From Collection Table
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Collections/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CollectionReadDTO>> GetCollection(int id)
        {
          if (_context.Collections == null)
          {
              return NotFound();
          }
            var collection = await _context.Collections.FindAsync(id);

            if (collection == null)
            {
                return NotFound();
            }

            return _mapper.Map<CollectionReadDTO>(collection);
        }

        /// <summary>
        /// Update the Collection by Id and name
        /// </summary>
        /// <param name="id"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        // PUT: api/Collections/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCollection(int id, Collection collection)
        {
            if (id != collection.CollectionId)
            {
                return BadRequest();
            }

            _context.Entry(collection).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CollectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add New Collection to collection Table
        /// </summary>
        /// <param name="collectionDto"></param>
        /// <returns></returns>
        // POST: api/Collections
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CollectionReadDTO>> PostCollection(CollectionCreateDTO collectionDto)
        {
          if (_context.Collections == null)
          {
              return Problem("Entity set 'ActorMoviesDbContext.Collections'  is null.");
          }

            var collectionModel = _mapper.Map<Collection>(collectionDto);

            _context.Collections.Add(collectionModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCollection", new { id = collectionModel.CollectionId }, collectionDto);
        }

        /// <summary>
        /// Delete The Collection from Collection Table
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Collections/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCollection(int id)
        {
            if (_context.Collections == null)
            {
                return NotFound();
            }
            var collection = await _context.Collections.FindAsync(id);
            if (collection == null)
            {
                return NotFound();
            }

            _context.Collections.Remove(collection);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CollectionExists(int id)
        {
            return (_context.Collections?.Any(e => e.CollectionId == id)).GetValueOrDefault();
        }
    }
}
