﻿namespace CinemaDock.DTOs.ActorDTO
{
    public class ActorCreateDTO
    {
        public string FullName { get; set; }
        public string? Alias { get; set; }
        public string Gender { get; set; }
        
    }
}
