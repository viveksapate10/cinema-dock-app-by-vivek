﻿namespace CinemaDock.DTOs.ActorDTO
{
    public class ActorReadDTO
    {
        public int ActorId { get; set; }
        public string FullName { get; set; }
        public string? Alias { get; set; }
        public string Gender { get; set; }
      
    }
}
