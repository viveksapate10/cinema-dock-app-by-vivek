﻿namespace CinemaDock.DTOs.CollectionDTO
{
    public class CollectionCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
