﻿using CinemaDock.DTOs.MovieDTO;

namespace CinemaDock.DTOs.CollectionDTO
{
    public class MovieCollectionReadDTO
    {
        public int CollectionId { get; set; }
        public string Name { get; set; }
        public List<MovieReadDTO> Movies { get; set; }
    }
}
