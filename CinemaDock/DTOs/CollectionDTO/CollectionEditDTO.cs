﻿namespace CinemaDock.DTOs.CollectionDTO
{
    public class CollectionEditDTO
    {
        public int CollectionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
