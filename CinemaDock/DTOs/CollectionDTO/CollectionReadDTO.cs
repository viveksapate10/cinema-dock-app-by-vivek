﻿namespace CinemaDock.DTOs.CollectionDTO
{
    public class CollectionReadDTO
    {
        public int CollectionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
       
        public List<int>? Movies { get; set; }
    }
}
