﻿using CinemaDock.DTOs.ActorDTO;

namespace CinemaDock.DTOs.CollectionDTO
{
    public class ActorCollectionReadDTO
    {
        public int CollectionId { get; set; }
        public string Name { get; set; }
        public List<ActorReadDTO> Actors { get; set; }
    }
}
