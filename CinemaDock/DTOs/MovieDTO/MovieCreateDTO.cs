﻿namespace CinemaDock.DTOs.MovieDTO
{
    public class MovieCreateDTO
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
      
        public int? CollectionId { get; set; }
    }
}
