﻿using CinemaDock.DTOs.ActorDTO;

namespace CinemaDock.DTOs.MovieDTO
{
    public class ActorsInMovieReadDTO
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public List<ActorReadDTO> Actors { get; set; }
    }
}
