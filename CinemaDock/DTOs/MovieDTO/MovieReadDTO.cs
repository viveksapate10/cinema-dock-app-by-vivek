﻿using CinemaDock.DTOs.CollectionDTO;

namespace CinemaDock.DTOs.MovieDTO
{
    public class MovieReadDTO
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
       
        public List<int> Actors { get; set; }
        public CollectionReadDTO Collection { get; set; }
    }
}
 