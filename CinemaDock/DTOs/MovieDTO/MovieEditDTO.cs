﻿namespace CinemaDock.DTOs.MovieDTO
{
    public class MovieEditDTO
    {
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
       
        public List<int> Actors { get; set; }
        public int CollectionId { get; set; }
    }
}
