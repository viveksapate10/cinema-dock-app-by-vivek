﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CinemaDock.Models
{
    [Table("Collections")]
    public class Collection
    {/// <summary>
     /// Auto incremented identifier for the Collection.
     /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key()]
        public int CollectionId { get; set; }

        /// <summary>
        /// The name of the Collection.
        /// </summary>
        [Required, MaxLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// The description of the Collection.
        /// </summary>
        [Required, MaxLength(255)]
        public string Description { get; set; }

        /// <summary>
        /// A collection of movies belonging to the Collection.
        /// Represents a one-to-many relationship.
        /// </summary>
        public ICollection<Movie> Movies { get; set; }
    }
}
