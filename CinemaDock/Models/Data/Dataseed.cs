﻿namespace CinemaDock.Models.Data
{
    public class Dataseed
    {
        public static List<Movie> GetMovies()
        {
            List<Movie> movies = new()
            {
                new Movie()
                {
                    MovieId = 1,
                    Title = "inside Out 1",
                    Genre = "Animation,Adventure",
                    ReleaseYear = 2015,
                    Director = "Pete Doctor",
                    CollectionId = 1
                },
                new Movie()
                {
                    MovieId = 2,
                    Title = "inside Out 2",
                    Genre = "Animation,Adventure",
                    ReleaseYear = 2016,
                    Director = "Pete Doctor",
                    CollectionId = 1
                },
                new Movie()
                {
                    MovieId = 3,
                    Title = "Star Wars-The Force Awakens Bigining",
                    Genre = "Action,Adventure",
                    ReleaseYear = 2014,
                    Director = " J.J.Abrams",
                    CollectionId = 2
                },
                new Movie()
                {
                    MovieId = 4,
                    Title = "Star Wars-The Force Awakens Conclusion",
                    Genre = "Action,Adventure",
                    ReleaseYear = 2016,
                    Director = "J.J.Abrams",
                    CollectionId = 2
                }
            };

            return movies;
        }

        public static List<Actor> GetActors()
        {
            List<Actor> actors = new()
            {
                new Actor()
                {
                    ActorId = 1,
                    FullName = "William hader",
                    Alias = "bill Hader",
                    Gender = "Male",
                },
                new Actor()
                {
                    ActorId = 2,
                    FullName = "Mark Hamill",
                    Alias = "Mark",
                    Gender = "Male",
                },
                new Actor()
                {
                    ActorId = 3,
                    FullName = "Anthony Daniels",
                    Alias = "Antho",
                    Gender = "Female",
                },
                new Actor()
                {
                    ActorId = 4,
                    FullName = "Harrison Ford",
                    Alias = "",
                    Gender = "Male",
                },
                new Actor()
                {
                    ActorId = 5,
                    FullName = "Peter Wilton",
                    Alias = "",
                    Gender = "Male",
                }
            };

            return actors;
        }

        public static List<Collection> GetCollections()
        {
            List<Collection> collections = new()
            {
                new Collection()
                {
                    CollectionId = 1,
                    Name = "inside Out",
                    Description = "An Animation and adventure series called inside out"
                },
                new Collection()
                {
                    CollectionId = 2,
                    Name = "Star Wars",
                    Description = "A series of action and adventure directed by J.J.Abrams "
                }
            };

            return collections;
        }
    }
}
