﻿using Microsoft.EntityFrameworkCore;

namespace CinemaDock.Models.Data
{
    public class ActorMoviesDbContext :DbContext
    {
        public ActorMoviesDbContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Setting the models to database tables.
        /// </summary>
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Collection> Collections { get; set; }

        /// <summary>
        /// On configuration we are setting the connection to the database,
        /// to further be able to perform queries and interact with it.
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }

        /// <summary>
        /// Seeded data to populate the database.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // TODO: add data from SeedData
            modelBuilder.Entity<Collection>().HasData(Dataseed.GetCollections());
            modelBuilder.Entity<Movie>().HasData(Dataseed.GetMovies());
            modelBuilder.Entity<Actor>().HasData(Dataseed.GetActors());

            modelBuilder.Entity<Actor>()
                .HasMany(m => m.Movies)
                .WithMany(c => c.Actors)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Actor>().WithMany().HasForeignKey("ActorId"),
                    je =>
                    {
                        je.HasKey("MovieId", "ActorId");
                        je.HasData(
                            new { MovieId = 1, ActorId = 1 },
                            new { MovieId = 2, ActorId = 1 },
                            new { MovieId = 2, ActorId = 2 },
                            new { MovieId = 2, ActorId = 3 },
                            new { MovieId = 3, ActorId = 4 },
                            new { MovieId = 3, ActorId = 5 },
                            new { MovieId = 4, ActorId = 4 },
                            new { MovieId = 4, ActorId = 5 }
                        );
                    });
        }
    }
}

