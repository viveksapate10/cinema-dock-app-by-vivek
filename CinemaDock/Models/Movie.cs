﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CinemaDock.Models
{
    [Table("Movies")]
    public class Movie
    {/// <summary>
     /// Auto incremented identifier for the movie.
     /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key()]
        public int MovieId { get; set; }

        /// <summary>
        /// The title of the movie.
        /// </summary>
        [Required, MaxLength(255)]
        public string Title { get; set; }

        /// <summary>
        /// The genre(s) of the movie. Genres should be separated with a comma.
        /// </summary>
        [Required, MaxLength(255)]
        public string Genre { get; set; }

        /// <summary>
        /// The year the movie was released.
        /// </summary>
        [Required, MaxLength(50)]
        public int ReleaseYear { get; set; }

        /// <summary>
        /// The director(s) of the movie. Directors should be separated with a comma.
        /// </summary>
        [Required, MaxLength(255)]
        public string Director { get; set; }

       

        /// <summary>
        /// A collection of characters that feature in the movie.
        /// </summary>
        public ICollection<Actor> Actors { get; set; } //M-M

        /// <summary>
        /// The unique identifier of the franchise the movie is a part of.
        /// </summary>
        public int? CollectionId { get; set; } //1-1

        /// <summary>
        /// The Collection the movie is a part of.
        /// Represents a one-to-many relationship
        /// </summary>
        public Collection Collection { get; set; } //1-M
    }
}
