﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CinemaDock.Models
{
    [Table("Actors")]
    public class Actor
    { /// <summary>
      /// Auto incremented identifier for the character.
      /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key()]
        public int ActorId { get; set; }

        /// <summary>
        /// Full name for the character.
        /// </summary>
        [Required, MaxLength(255)]
        public string FullName { get; set; }

        /// <summary>
        /// Alias for the character, can be empty if not applicable.
        /// </summary>
        [MaxLength(255)]
        public string? Alias { get; set; }

        /// <summary>
        /// Gender of the character.
        /// </summary>
        [Required, MaxLength(50)]
        public string Gender { get; set; }


        /// <summary>
        /// A collection of movies the character features in.
        /// Represents a many-to-many relationship.
        /// </summary>
        public ICollection<Movie> Movies { get; set; }
    }
}

