# Project Tittle-
Cinema Dock App

## Description
Cinema Dock Management is the  Web API project to Manage the Actors, Movies and collections. It includes the Data Transfer Objects ,CRUD operations , Database migration having connections  as one to one ,one to many  and many to many (Relational Database) 


# Developed by-
Vivek Mahaveer Sapate
Email:viveksapate10@gmail.com
Phone:+91-7411548299

## Getting Started

### Installing

* Download this source file (https://gitlab.com/viveksapate10/cinema-dock-app-by-vivek.git) into local machine and Open in visual studio.

* After opening this project go to appsetting class>>default connection>>Rename this server name (Server=DESKTOP-L2EHC8K\\SQLEXPRESS) as yours database server name.

### Dependencies
Packages ,that are already installed in this project

*Microsoft.EntityFramework.Core.sqlserver

*Microsoft.EntityFramework.Core.Design

*Microsoft.EntityFramework.Core.Tools

*autoMapper.Extension.Microsoft.DependencyInjection

Note-No need to install other packages to run this project.

### Executing program

*On the top menu bar >>Debug>>Start Debugging (Results in opening with browser)

*Here ,we can prform CRUD (create ,read,update and delete)operation

# Challenges Faced
*Difficulty to find the proper data.

*Difficulty to maintain relational logic.

## Help

Any advise for common problems or issues.
command to run if program contains helper info
-
